var express = require('express');
var router = express.Router();
var path = require('path');
var User = require('../models/user');
var Post = require('../models/post');

router.get('/', function(req, res, next){
	return res.sendFile(path.join(__dirname,'../views/login.html'));
});

router.get('/register', function(req, res, next){
	return res.sendFile(path.join(__dirname,'../views/register.html'));
});

router.post('/register', function(req, res, next){
	// password confirmation
	if (req.body.password !== req.body.password_confirmation) {
		var err = new Error('Passwords do not match.');
		err.status = 400;
		// res.send("Passwords dont match");
		return next(err);
	}
	if (req.body.email && req.body.username && req.body.password && req.body.password_confirmation) 
	{
		var userData = {
			email: req.body.email,
			username: req.body.username,
			password: req.body.password,
		}

		User.create(userData, function (err, user) {
			if (err) {
				return next(err)
			} else {
				return res.redirect('/');
			}
		});
	} 
	else
	{
		var err = new Error('All fields are required.');
		err.status = 400;
		return next(err);
	}
});

router.post('/login', function(req, res, next){
	if (req.body.email && req.body.password) 
	{
		User.authenticate(req.body.email, req.body.password, function (error, user){
			if (error || !user) {
				var err = new Error('Wrong email or password');
				err.status = 401;
				return next(err);
			} 
			else
			{
				req.session.userId = user._id;
				return res.redirect('/profile');
			}
		});
	}
	else
	{
		var err = new Error('All fields are required.');
		err.status = 400;
		return next(err);
	}
});

router.get('/profile', function (req, res, next){
	User.findById(req.session.userId).exec(function (error, user){
		if (error)
		{
			return next(error);
		}
		else if (user === null)
		{
			var err = new Error('Not authorized.');
			err.status = 400;
			return next(err);
		}
		else
		{
			var posts = '';
			var cursor = Post.find({ user_id: user._id}).cursor();
			cursor.on('data', function(doc) {
				posts += '<p>' + doc.text + '</p><form action="/post/' + doc._id + '" method="POST"><input type="submit" value="Delete"></form>';
			});

			cursor.on('close', function() {
				return res.send('<h2>Welcome: ' + user.username + '</h2><a href="/logout">Logout</a>' + 
					'<form action="/post" method="POST"><input type="text" name="post">' + 
					'<input type="submit" name="submit" value="Post"></form>' + posts);
			});
		}
	});
});

router.get('/logout', function (req, res, next){
	if (req.session) 
	{
		req.session.destroy(function (err){
			if (err) 
			{
				return next(err);
			}
			else
			{
				return res.redirect('/');
			}
		});
	}
});

router.post('/post', function (req, res, next){
	User.findById(req.session.userId).exec(function (error, user){
		if (error)
		{
			return next(error);
		}
		else if (user === null)
		{
			var err = new Error('Not authorized.');
			err.status = 400;
			return next(err);
		}
		else
		{
			var postData = {
				text: req.body.post,
				user_id: user._id,
			}

			Post.create(postData, function (err, post) {
			if (err) {
				return next(err)
			} else {
				return res.redirect('/profile');
			}
		});
		}
	});
});

router.post('/posts/:post_id', function (req, res, next){
	console.log(req.post_id);
});
// // middlewares
// function requiresLogin(req, res, next) {
// 	if (req.session && req.session.userId) 
// 	{
// 		return next();
// 	}
// 	else
// 	{
// 		var err = new Error('You must be logged in to view this page.');
// 		err.status = 401;
// 		return next(err);
// 	}
// }

module.exports = router;