var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

// DB connection
mongoose.connect('mongodb://localhost/testDB');
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
	console.log('connected to db');
});

// use session
app.use(session({
	secret: 'splendex',
	resave: true,
	saveUninitialized: false,
	store: new MongoStore({
    	mongooseConnection: db
  	})
}));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Routes
var routes = require('./routes/router');
app.use('/', routes);


// Error 404
app.use(function(req, res, next){
	var err = new Error('File not found.');
	err.status = 404;
	next(err);
});


// Errors
app.use(function(err, req, res, next){
	res.status(err.status || 500);
	res.json({
		message: err.message,
		error: err.status
	});
});

// listen on port 8000
app.listen(8000, function(){
	console.log('listening on port 8000');
});