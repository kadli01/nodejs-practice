var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
	text: {
		type: String,
		required: true,
		trim: true
	},
	user_id: {
		type: String,
		required: true,
	}
});

var Post = mongoose.model('Post', PostSchema);
module.exports = Post;